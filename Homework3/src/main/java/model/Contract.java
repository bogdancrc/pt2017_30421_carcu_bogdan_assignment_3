package model;
/**
 * 
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *
 */
public class Contract {

	private int productId;
	private int orderId;
	private int quantity;
	
	public Contract() {
		
	}
	
	public Contract(int productId, int orderId, int quantity) {
		
		this.setProductId(productId);
		this.setOrderId(orderId);
		this.setQuantity(quantity);
		
	}
	
	public Contract(int orderId, int quantity) {
		
		this.setOrderId(orderId);
		this.setQuantity(quantity);
		
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String toString() {
		
		return "Contract [productId = " + productId + ", orderId = " + orderId + ", quantity = " + quantity + "]";
		
	}
	
}
