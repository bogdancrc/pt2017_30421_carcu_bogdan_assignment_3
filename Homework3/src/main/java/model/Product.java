package model;
/**
 * 
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *
 */
public class Product {

	private int productId;
	private String name;
	private int price;
	private int stock;
	
	public Product() {
		
	}
	
	public Product(int productId, String name, int price, int stock) {
		
		this.setProductId(productId);
		this.setName(name);
		this.setPrice(price);
		this.setStock(stock);
		
	}
	
	public Product(String name, int price, int stock) {

		this.setName(name);
		this.setPrice(price);
		this.setStock(stock);
		
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public String toString() {
		
		return "Product [productId = " + productId + ", name = " + name + ", price = " + price + ", stock = " + stock + "]";
		
	}
	
}
