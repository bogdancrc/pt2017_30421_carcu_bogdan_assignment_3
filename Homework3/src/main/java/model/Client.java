package model;

/**
 * 
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *
 */
public class Client {
	
	private int clientId;
	private String name;
	private String address;
	private int age;
	
	public Client() {
		
	}
	
	public Client(int clientId, String name, String address, int age) {

		
		this.setClientId(clientId);
		this.setName(name);
		this.setAddress(address);
		this.setAge(age);
		
	}
	
	public Client(String name, String address, int age) {
		
	
		this.setName(name);
		this.setAddress(address);
		this.setAge(age);
		
	}
	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString() {
		
		return "Client [clientId = " + clientId + ", name = " + name + ", address =  " + address + ", age = " + age + "]";
		
	}

}
