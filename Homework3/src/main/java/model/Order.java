package model;
/**
 * 
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *
 */
public class Order {

	private int orderId;
	private int clientId;
	private int sum;
	
	public Order() {
		
	}
	
	public Order(int orderId, int clientId, int sum) {
		
		this.setOrderId(orderId);
		this.setClientId(clientId);
		this.setSum(sum);
		
	}
	
	public Order(int clientId, int sum) {
		
		this.setClientId(clientId);
		this.setSum(sum);
		
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}
	
	public String toString() {
		
		return "Order [orderId = " + orderId + ", clientId = " + clientId + ", sum = " + sum + "]";
		
	}
	
}
