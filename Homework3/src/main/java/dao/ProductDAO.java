package dao;

import java.util.List;
import model.Product;

/**
 * Child of AbstractDAO class.
 * Specialized in operations involving Product objects
 * 
 * @author Carcu Bogdan
 * @since May 2017
 * 
 */
public class ProductDAO extends AbstractDAO<Product>{

	public Product findProduct(int id) {
		
		ProductDAO dao = new ProductDAO();
		return dao.findById(id);
		
	}
	
	public List<Product> findAllProducts() {
		
		ProductDAO dao = new ProductDAO();
		return dao.findAll();
		
	}
	
	public void insertProduct(Product p) {
		
		ProductDAO dao = new ProductDAO();
		dao.insert(p);
		
	}
	
	public boolean deleteProduct(int id) {
		
		ProductDAO dao = new ProductDAO();
		return dao.delete(id);
		
	}
	
	public void updateProduct(int id, Product p) {
		
		ProductDAO dao = new ProductDAO();
		dao.update(id, p);
		
	}
	
}
