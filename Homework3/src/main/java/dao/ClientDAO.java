package dao;

import java.util.List;
import model.Client;
/**
 * Child of AbstractDAO class.
 * Specialized in operations involving Client objects
 * 
 * @author Carcu Bogdan
 * @since May 2017
 *
 */
public class ClientDAO extends AbstractDAO<Client>{

	public Client findClient(int id) {
		
		ClientDAO dao = new ClientDAO();
		return dao.findById(id);
		
	}
	
	public List<Client> findAllClients() {
		
		ClientDAO dao = new ClientDAO();
		return dao.findAll();
		
	}
	
	public void insertClient(Client c) {
		
		ClientDAO dao = new ClientDAO();
		dao.insert(c);
		
	}
	
	public boolean deleteClient(int id) {
		
		ClientDAO dao = new ClientDAO();
		return dao.delete(id);
		
	}
	
	public void updateClient(int id, Client c) {
		
		ClientDAO dao = new ClientDAO();
		dao.update(id, c);
		
	}
	
}
