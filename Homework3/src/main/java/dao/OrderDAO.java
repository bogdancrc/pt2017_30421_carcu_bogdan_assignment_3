package dao;

import java.util.List;
import model.Order;
/**
 * 
 * Child of AbstractDAO class.
 * Specialized in operations involving Order objects
 * 
 * @author Carcu Bogdan
 * @since May 2017
 *
 */
public class OrderDAO extends AbstractDAO<Order> {

	public Order findOrder(int id) {
		
		OrderDAO dao = new OrderDAO();
		return dao.findById(id);
		
	}
	
	public List<Order> findAllOrders() {
		
		OrderDAO dao = new OrderDAO();
		return dao.findAll();
		
	}
	
	public void insertOrder(Order o) {
		
		OrderDAO dao = new OrderDAO();
		dao.insert(o);
		
	}
	
	public boolean deleteOrder(int id) {
		
		OrderDAO dao = new OrderDAO();
		return dao.delete(id);
		
	}
	
	public void updateOrder(int id, Order o) {
		
		OrderDAO dao = new OrderDAO();
		dao.update(id, o);
		
	}
	
}
