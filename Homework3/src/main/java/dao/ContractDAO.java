package dao;

import java.util.List;
import model.Contract;
/**
 * 
 * Child of AbstractDAO class.
 * Specialized in operations involving Contract objects
 * 
 * @author Carcu Bogdan
 * @since May 2017
 *
 */
public class ContractDAO extends AbstractDAO<Contract>{
	
	public Contract findContract(int id) {
		
		ContractDAO dao = new ContractDAO();
		return dao.findById(id);
		
	}
	
	public List<Contract> findAllContracts() {
		
		ContractDAO dao = new ContractDAO();
		return dao.findAll();
		
	}
	
	public void insertContract(Contract c) {
		
		ContractDAO dao = new ContractDAO();
		dao.insert(c);
		
	}
	
	public boolean deleteContract(int id) {
		
		ContractDAO dao = new ContractDAO();
		return dao.delete(id);
		
	}
	
	public void updateContract(int id, Contract c) {
		
		ContractDAO dao = new ContractDAO();
		dao.update(id, c);
		
	}

}
