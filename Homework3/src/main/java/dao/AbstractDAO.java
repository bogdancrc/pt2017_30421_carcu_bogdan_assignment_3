package dao;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import connection.ConnectionFactory;
/**
 * Generic class that uses reflection and query-specific methods
 * in order to implement CRUD operations.
 * 
 * @author Carcu Bogdan
 * @since May 2017
 * 
 */
public class AbstractDAO<T> {
	
	private final Class<T> type;
	
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		
		this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		
	}

	/**
	 * @return An array of objects representing the fields of the input's class
	 */
	private Object[] retrieveProperties(Object object) {

		Object[] result = new Object[object.getClass().getDeclaredFields().length];
		int c = 0;
		
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); // set modifier to public
			Object value;
			try {
				value = field.get(object);
				result[c++] = value;

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
		return result;
	}
	
	//Creation of different queries
	
	private String createSelectQuery(String field) {
		
		if(type.getSimpleName().equals("Order"))
			return "SELECT * FROM " + type.getSimpleName().toLowerCase() + "_table WHERE " + field + " = ?";
		
		return "SELECT * FROM " + type.getSimpleName() + " WHERE " + field + " = ?";
	}
	
	private String createInsertQuery() {
		
		String result = "INSERT INTO ";
		
		if(type.getSimpleName().equals("Order"))
			result += type.getSimpleName().toLowerCase() + "_table (";
		else
			result += type.getSimpleName() + " (";
		
		for(int i = 0; i < type.getDeclaredFields().length; ++i) {
			result += type.getDeclaredFields()[i].getName();
		if(i != type.getDeclaredFields().length - 1)
			result += ", ";
		}
		result += ") VALUES(";
		for(int i = 0; i < type.getDeclaredFields().length - 1; ++i)
			result += "?,";
		result += "?)";
		System.out.println(result);
		return result;
	}
	
	private String createDeleteQuery(String field) {
		
		if(type.getSimpleName().equals("Order"))
			return "DELETE FROM " + type.getSimpleName().toLowerCase() + "_table WHERE " + field + " = ?";
		return "DELETE FROM " + type.getSimpleName() + " WHERE " + field + " = ?";
	}
	
	private String createUpdateQuery(String field, T item) {
		
		String result;
		if(type.getSimpleName().equals("Order"))
			result = "UPDATE " + type.getSimpleName().toLowerCase() + "_table SET ";
		else
			result = "UPDATE " + type.getSimpleName() + " SET ";
		
		for(int i = 0; i < type.getDeclaredFields().length; ++i) {
			result += type.getDeclaredFields()[i].getName();	
			result += " = ";
			result += "'" + retrieveProperties(item)[i] + "'";
			if(i != type.getDeclaredFields().length - 1)
				result += ", ";
		}
		result += " WHERE " + field + " = ?";
		System.out.println(result);
		return result;
	}
	
	/**
	 * @param resultSet The result set of executing a query
	 * @return A list of objects depicting the result set entries
	 */
	private List<T> createObjects(ResultSet resultSet) {
		
		List<T> list = new ArrayList<T>();
		
		try{
				while(resultSet.next()) {
				
				T instance = type.newInstance();
				Object val;
				for(Field field : type.getDeclaredFields()) {
					
				    val = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method m = propertyDescriptor.getWriteMethod();
					m.invoke(instance, val);
					
				}
				list.add(instance);
			}		
				return list;
				
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * @param id The id of a specific item
	 * @return The first row after query execution, in the shape of the generic type T
	 */
	public T findById(int id) {
		
		Connection connection = null;
		PreparedStatement findStatement = null;
		ResultSet resultSet = null;
		String query;
		
		if(!type.getSimpleName().equals("Contract"))
			query = createSelectQuery(type.getSimpleName().toLowerCase() + "Id");
		else
			query = createSelectQuery("orderId");
		
		try {
			
			connection = ConnectionFactory.getConnection();
			findStatement = connection.prepareStatement(query);
			findStatement.setString(1, id + "");
			resultSet = findStatement.executeQuery();			
			
			return createObjects(resultSet).get(0);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(connection);
			
		}
		return null;
	}
	
	/**
	 * @return All table entries resulted from query execution in the shape of a list.
	 */
	public List<T> findAll() {
		
		Connection connection = null;
		PreparedStatement findAllStatement = null;
		ResultSet resultSet = null;
		String query;
		if(type.getSimpleName().equals("Order"))
			query = "SELECT * FROM " + type.getSimpleName().toLowerCase() + "_table";
		else
			query = "SELECT * FROM " + type.getSimpleName();
		
		try {
			
			connection = ConnectionFactory.getConnection();
			findAllStatement = connection.prepareStatement(query);
			resultSet = findAllStatement.executeQuery();
			
			return createObjects(resultSet);
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(findAllStatement);
			ConnectionFactory.close(connection);
			
		}
		return null;
	}

	 /**
	  * @param type Element to be inserted
	  */
	public void insert(T type) {
		
		Connection connection = null;
		PreparedStatement insertStatement = null;
		
		try {
			
			connection = ConnectionFactory.getConnection();
			insertStatement = connection.prepareStatement(createInsertQuery());
			Field[] fs = type.getClass().getDeclaredFields();
			
			for(int i = 0; i < fs.length; ++i) {
				
				fs[i].setAccessible(true);
				insertStatement.setString(i + 1, fs[i].get(type).toString());
			}
			
			insertStatement.executeUpdate();	
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);	
		}
	}

	/**
	 * @param id Element to be deleted
	 * @return true if the element was successfully deleted, false otherwise
	 */
	public boolean delete(int id) {
		
		Connection connection = null;
		PreparedStatement deleteStatement = null;
		String query;
		
		if(!type.getSimpleName().equals("Contract"))
			query = createDeleteQuery(type.getSimpleName().toLowerCase() + "Id");
		else
			query = createDeleteQuery("orderId");
		
		try {
			
			connection = ConnectionFactory.getConnection();
			deleteStatement = connection.prepareStatement(query);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();
			return true;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(connection);
			
		}
		return false;
	}
	
	/**
	 * @param id The id of the element to be updated
	 * @param item The actual "replacement" of the element to be updated
	 */
	public void update(int id, T item) {
		
		Connection connection = null;
		PreparedStatement updateStatement = null;
		String query;
		
		if(!type.getSimpleName().equals("Contract"))
			query = createUpdateQuery(type.getSimpleName().toLowerCase() + "Id", item);
		else
			query = createUpdateQuery("orderId", item);
		
		try {
		
			connection = ConnectionFactory.getConnection();
			updateStatement = connection.prepareStatement(query);
			updateStatement.setString(1, id + "");
			updateStatement.executeUpdate();	
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(connection);
		}
	}
}
