package start;
/**
 * 
 * Main application launcher
 * 
 */
import java.sql.SQLException;
import presentation.Controller;
import presentation.View;

public class Start {
	public static void main(String[] args) throws SQLException {
		
		new Controller(new View());
	
	}
}
