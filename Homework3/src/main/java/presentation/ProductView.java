package presentation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import bll.ProductBLL;
import model.Product;
/**
 * 
 * Class that provides both a GUI for the product distribution window and its 
 * functionality/interactivity.
 * 
 * @author Carcu Bogdan 
 * 
 */
@SuppressWarnings("serial")
public class ProductView extends JFrame{
	
	private JTable theTable;
	private JScrollPane tablePane;
	private JPanel p, p1, p2, p3;
	private JButton add, delete, update,view;
	private JTextField name, price, stock;
	private ProductBLL productBll;
	
	public ProductView() {
		
		super("Product distribution");
		setLayout(new FlowLayout());
		productBll = new ProductBLL();
		
		p = new JPanel(); p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		add = new JButton("Add"); delete = new JButton("Delete"); update = new JButton("Update"); view = new JButton("Refresh");
		p1.add(add); p1.add(delete); p1.add(update); p1.add(view);
		name = new JTextField("Name", 15); price = new JTextField("Price", 15); stock = new JTextField("Stock", 15);
		p2.add(name); p2.add(price); p2.add(stock);
		
		theTable = TableCreator.createTable(productBll.findAll());
		tablePane = new JScrollPane(theTable);
		p3.add(tablePane, BorderLayout.CENTER);
		
		p.add(p1);
		p.add(p2);
		p.add(p3);
		add(p);	
		
		setAddListener();
		setRefreshListener();
		setDeleteListener();
		setUpdateListener();
		
		setSize(560, 550);
		setResizable(false);
		setVisible(true);
	}
	
	/**
	 * Refreshes the working table
	 */
	private void refreshTable() {
		
		theTable = TableCreator.createTable(productBll.findAll());
		JScrollPane pane = new JScrollPane(theTable);
		
		p3.invalidate();
		p3.removeAll();
		p3.add(pane);
		p3.revalidate();
			
	}
	 
	/**
	 * Sets functionality to the "Add to cart" button
	 */
	public void setAddListener() {
		
		add.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
			try{	
				
				Product p = new Product(name.getText(), Integer.parseInt(stock.getText()), Integer.parseInt(stock.getText()));
				productBll.insert(p);
				refreshTable();
				
			} catch (IllegalArgumentException ex) {
				
				sendErrorMsg(ex.getMessage());
				
			
			} catch(Exception ex) {
				
				sendErrorMsg("Failed to insert! Incorrect input data.");
				
			}
				
			}
				
		});
		
	}
	
	/**
	 * Sets functionality to the "Delete" button
	 */
	public void setDeleteListener() {
		
		delete.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				int row, id = 0;
				
				try{
				
				 row = theTable.getSelectedRow();
				 id = (Integer) theTable.getValueAt(row, 0);
				 
				} catch (Exception ex) {
					
					sendErrorMsg("No item selected.");
					
				}
				
				if(productBll.delete(id) == false)
					sendErrorMsg("Cannot delete item being purchased");
				
				refreshTable();
			}
				
		});
		
	}
	/**
	 * Sets functionality to the "Update" button
	 */
	public void setUpdateListener() {
		
		update.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
			try{	
				int row = theTable.getSelectedRow();
				
				int id = (Integer) theTable.getValueAt(row, 0);
				String name = (String) theTable.getValueAt(row, 1);
			
				int price;
				if(theTable.getValueAt(row, 2) instanceof String)
					price = Integer.parseInt((String)theTable.getValueAt(row, 2));
				else
					price = (Integer) theTable.getValueAt(row, 2);
				
				int stock;
				if(theTable.getValueAt(row, 3) instanceof String)
					stock = Integer.parseInt((String)theTable.getValueAt(row, 3));
				else
					stock = (Integer) theTable.getValueAt(row, 3);
				
				Product p = new Product(id, name, price, stock);
				productBll.update(id, p);
				refreshTable();
			} catch (IllegalArgumentException ex) {
				
				sendErrorMsg(ex.getMessage());
				
			}
			}
		});
 }
	
	/**
	 * Sets functionality to the "Refresh" button
	 */
	public void setRefreshListener() {
		
		view.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				refreshTable();
		
			}
				
		});
		
	}
	
	/**
	 * @param msg Plain error message
	 */
	private void sendErrorMsg(String msg){
		
		JOptionPane.showMessageDialog(null, msg, "Oops!", JOptionPane.ERROR_MESSAGE);
	}
	
}
