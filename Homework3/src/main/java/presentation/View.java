package presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * 
 * The view of MVC structure. (Main view)
 * @author Carcu Bogdan
 * @since May 2017
 * 
 * 
 */
@SuppressWarnings("serial")
public class View extends JFrame{

	private JPanel p, p1, p2, p3, p4, p5;
	private JLabel welcome1, welcome2;
	private JButton clientBtn, productBtn, orderBtn;
	
	public View() {
		
		super("Blue Warehouse");
		
		p = new JPanel(); p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel(); p4 = new JPanel(); p5 = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		welcome1 = new JLabel("Welcome!");
		welcome2 = new JLabel("Please select the wanted category:");
		clientBtn = new JButton("Clients");
		productBtn = new JButton("Products");
		orderBtn = new JButton("Order Process");
		
		p1.add(welcome1, BorderLayout.CENTER);
		p2.add(welcome2, BorderLayout.CENTER);
		p3.add(clientBtn, BorderLayout.CENTER);
		p4.add(productBtn, BorderLayout.CENTER);
		p5.add(orderBtn, BorderLayout.CENTER);
		p.add(p1); p.add(p2); p.add(p3); p.add(p4); p.add(p5);
		add(p);
		
		setSize(250, 240);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public void setClientListener(ActionListener listenForButton) {
		
		clientBtn.addActionListener(listenForButton);
		
	}
	
	public void setProductListener(ActionListener listenForButton) {
		
		productBtn.addActionListener(listenForButton);
		
	}
	
	public void setOrderListener(ActionListener listenForButton) {
		
		orderBtn.addActionListener(listenForButton);
		
	}
	
}
