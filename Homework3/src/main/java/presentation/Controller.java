package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * 
 * The controller of MVC structure.
 * @author Carcu Bogdan
 * @since May 2017
 * 
 * 
 */
public class Controller {
	
	private View theView;
	
	public Controller(View theView) {
		
		this.theView = theView;
		
		this.theView.setClientListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				new ClientView();
			}
				
		});
		
		this.theView.setProductListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				new ProductView();
			}
				
		});
		
		this.theView.setOrderListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				new OrderView();
			}
				
		});
		 
	}

}
