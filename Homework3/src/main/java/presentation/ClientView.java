package presentation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import bll.ClientBLL;
import model.Client;
/**
 * Class that provides both a GUI for the client administration window and its 
 * functionality/interactivity. 
 * 
 * @author Carcu Bogdan
 * 
 */
@SuppressWarnings("serial")
public class ClientView extends JFrame{

	private JTable theTable;
	private JScrollPane tablePane;
	private JPanel p, p1, p2, p3;
	private JButton add, delete, update,view;
	private JTextField name, address, age;
	private ClientBLL clientBll;
	
	public ClientView() {
		
		super("Client administration");
		setLayout(new FlowLayout());
		clientBll = new ClientBLL();
		
		p = new JPanel(); p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		add = new JButton("Add"); delete = new JButton("Delete"); update = new JButton("Update"); view = new JButton("Refresh");
		p1.add(add); p1.add(delete); p1.add(update); p1.add(view);
		name = new JTextField("Name", 15); address= new JTextField("Address", 15); age = new JTextField("Age", 15);
		p2.add(name); p2.add(address); p2.add(age);
		
		theTable = TableCreator.createTable(clientBll.findAll());
		tablePane = new JScrollPane(theTable);
		p3.add(tablePane, BorderLayout.CENTER);
		
		p.add(p1);
		p.add(p2);
		p.add(p3);
		add(p);	
		
		setAddListener();
		setRefreshListener();
		setDeleteListener();
		setUpdateListener();
		
		setSize(560, 550);
		setResizable(false);
		setVisible(true);
	}
	
	 /**
	  *  Resets the view of the table, in order
	  *  to keep it updated in a clean way.
	  */
	private void refreshTable() {
		
		theTable = TableCreator.createTable(clientBll.findAll());
		JScrollPane pane = new JScrollPane(theTable);
		
		p3.invalidate();
		p3.removeAll();
		p3.add(pane);
		p3.revalidate();
			
	}
	 
	/**
	 * Sets functionality to the "Add" button
	 */
	public void setAddListener() {
		
		add.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
			try{	
				Client c = new Client(name.getText(), address.getText(), Integer.parseInt(age.getText()));
				clientBll.insert(c);
				refreshTable();
				
			} catch (IllegalArgumentException ex) {
				
				sendErrorMsg(ex.getMessage());
				
				
			} catch(Exception ex) {
				
				sendErrorMsg("Failed to insert! Incorrect input data.");
				
			}
			
		}
				
	});
		
}
	/**
	 * Sets functionality to the "Delete" button
	 */
	public void setDeleteListener() {
		
		delete.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				int row, id = 0;
				
			try {
				
				 row = theTable.getSelectedRow();
				 id = (Integer) theTable.getValueAt(row, 0);
				
			} catch (Exception ex) {
				
				sendErrorMsg("No client chosen.");
				
			}
				
				if (clientBll.delete(id) == false)
					sendErrorMsg("Cannot delete client in existing tranzaction!");
				
				refreshTable();
		
		}
				
	});
		
}
	/**
	 * Sets functionality to the "Update" button
	 */
	public void setUpdateListener() {
		
		update.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
			try{
				int row = theTable.getSelectedRow();
				
				int id = (Integer) theTable.getValueAt(row, 0);
				String name = (String) theTable.getValueAt(row, 1);
				String address = (String) theTable.getValueAt(row, 2);
				int age;
				if(theTable.getValueAt(row, 3) instanceof String)
					age = Integer.parseInt((String)theTable.getValueAt(row, 3));
				else
					age = (Integer) theTable.getValueAt(row, 3);
			
				Client c = new Client(id, name, address, age);
	
				clientBll.update(id, c);
				refreshTable();
				
			} catch (IllegalArgumentException ex) {
				
				sendErrorMsg(ex.getMessage());
				
			} catch (Exception ex) {
				
				sendErrorMsg("Cannot update this row!");
				
			}
		} 
	});
 }
	
	/**
	 * Sets functionality to the "Refresh" button
	 */
	public void setRefreshListener() {
		
		view.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				refreshTable();
		
			}
				
		});
		
	}
	
	/**
	 * @param msg Plain error message to be signaled.
	 */
	private void sendErrorMsg(String msg){
		
		JOptionPane.showMessageDialog(null, msg, "Oops!", JOptionPane.ERROR_MESSAGE);
	}
	
}
