package presentation;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import bll.ClientBLL;
import bll.ProductBLL;
import bll.process.OrderProcessing;
import model.Client;
import model.Product;
/**
 * Class that provides both a GUI for the order processing window and its 
 * functionality/interactivity. 
 * @author Carcu Bogdan
 * 
 */
@SuppressWarnings("serial")
public class OrderView extends JFrame{

	private ProductBLL productBll;
	private ClientBLL clientBll;
	private JTable productTable, clientTable;
	private JLabel ord;
	private JLabel quant;
	private JTextField quantityField;
	private JPanel p, p1, p2, p3;
	private JButton makeOrder, addToCart;
	private JScrollPane pane1, pane2;
	private OrderProcessing orderProcessing;

	private Client myClient;
	private List<Product> products;
	private List<Integer> quantities;
	
	public OrderView() {
		
		super("Order process");
		setLayout(new FlowLayout());
		
		products = new ArrayList<Product>();
		quantities = new ArrayList<Integer>();
		productBll = new ProductBLL();
		clientBll = new ClientBLL();
		orderProcessing = new OrderProcessing();
		p = new JPanel(); 
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel();
		makeOrder = new JButton("Make order");
		addToCart = new JButton("Add to cart");
		
		setCartListener();
		setOrderListener();
		
		ord = new JLabel("Link, insert quantity, add to cart, and make an order whenever you want!");
		productTable = TableCreator.createTable(productBll.findAll());
		clientTable = TableCreator.createTable(clientBll.findAll());
		
		pane1 = new JScrollPane(productTable);
		pane2 = new JScrollPane(clientTable);
		
		quant = new JLabel("Insert quantity: ");
		quantityField = new JTextField(5);
		
		p1.add(ord); p1.add(makeOrder);
		p3.add(quant); p3.add(quantityField); p3.add(addToCart);
		p2.add(pane2); p2.add(pane1); 
		
		p.add(p1); p.add(p3);p.add(p2);
		add(p);
		
		setSize(970, 560);
		setVisible(true);
		setResizable(false);
	}
	
	/**
	 * Sets the client that makes requests
	 */
	private void getClient() {
		
		int row = clientTable.getSelectedRow();
		
		int id = (Integer) clientTable.getValueAt(row, 0);
		String name = (String) clientTable.getValueAt(row, 1);
		String address = (String) clientTable.getValueAt(row, 2);
		int age;
		if(clientTable.getValueAt(row, 3) instanceof String)
			age = Integer.parseInt((String)clientTable.getValueAt(row, 3));
		else
			age = (Integer) clientTable.getValueAt(row, 3);
	
		myClient = new Client(id, name, address, age);
		
	}
	
	/**
	 * Adds to wishlist a product
	 */
	private void getProduct() {
		
		int row = productTable.getSelectedRow();
		
		int id = (Integer) productTable.getValueAt(row, 0);
		String name = (String) productTable.getValueAt(row, 1);
	
		int price;
		if(productTable.getValueAt(row, 2) instanceof String)
			price = Integer.parseInt((String)productTable.getValueAt(row, 2));
		else
			price = (Integer) productTable.getValueAt(row, 2);
		
		int stock;
		if(productTable.getValueAt(row, 3) instanceof String)
			stock = Integer.parseInt((String)productTable.getValueAt(row, 3));
		else
			stock = (Integer) productTable.getValueAt(row, 3);
		
		Product p = new Product(id, name, price, stock);
		products.add(p);
		
	}
	
	/**
	 * Gets the wanted quantity
	 */
	private void getQuantity() {
		
		
		int q = Integer.parseInt(quantityField.getText());
		quantities.add(q);

		
	}
	
	/**
	 * Sets functionality to the "Add to cart" button
	 */
	public void setCartListener() {
		
		addToCart.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
			try{
				getClient();
				getProduct();
				getQuantity();
				sendMsg("Added to cart!");
				
			
			} catch (Exception ex) {
				
				sendErrorMsg("Adding to cart failed!");
				
			}
		}
				
		});
		
	}
	
	/**
	 * Sets functionality to the "Make order" button
	 */
	public void setOrderListener() {
		
		makeOrder.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
			try{
				orderProcessing.makeOrder(myClient, products, quantities);
				sendMsg("Order was made successfully!");
				refreshTable();
				products.clear();
				quantities.clear();
			} catch (IllegalArgumentException ez) {
				
				sendErrorMsg(ez.getMessage());
				
			} catch (Exception ex) {
				
				sendErrorMsg("Order failed!");
				
			}
				
		}
				
	});
		
}
	/**
	 * Refreshes the product table accordingly
	 */
	private void refreshTable() {
		
		p2.invalidate();
		p2.remove(pane1);
		
		productTable = TableCreator.createTable(productBll.findAll());
		pane1 = new JScrollPane(productTable);
		
		p2.add(pane1);
		p2.revalidate();
			
	}
	
	/**
	 * @param msg Plain message
	 */
	private void sendErrorMsg(String msg){
		
		JOptionPane.showMessageDialog(null, msg, "Oops!", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * @param msg Plain error message
	 */
	private void sendMsg(String msg) {
		
		JOptionPane.showMessageDialog(null, msg, "Confirmation", JOptionPane.INFORMATION_MESSAGE);
		
	}

}
