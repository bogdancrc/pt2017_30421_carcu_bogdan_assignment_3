package bll.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import bll.ContractBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Contract;
import model.Order;
import model.Product;

/**
 * 
 * Class used as a part of the business layer.
 * Its main purpose is the computing of steps necessary in making an order.
 * Creates a bill which follows a specific order.
 * 
 * @author Carcu Bogdan
 * @since May 2017
 * 
 */

public class OrderProcessing {

	private static int billNumber = 1;
	
	private OrderBLL orderBLL;
	private ContractBLL contractBLL;
	private ProductBLL productBLL;
	private PrintWriter writer;
	
	public OrderProcessing() {
		
		orderBLL = new OrderBLL();
		contractBLL = new ContractBLL();
		productBLL = new ProductBLL();
		
	}
	
	/**
	 * @param product Name of product
	 * @param quantity Quantity to extract
	 * @param sum The total cost of the transaction
	 */
	private void writeBill(String product, int quantity, int sum) {
					
			writer.println("Product: " + product);
		    writer.println("Quantity: " + quantity + " item(s)");
			writer.println("Cost: " + sum + " $");
			writer.println("--------------------------------------");	      
	}
	
	/**
	 * Method that both takes the responsibility of making an order and writing a bill.
	 * 
	 * @param client The client requesting products
	 * @param products The set of products on the client's wishlist
	 * @param quantities The quantities of each product to be extracted
	 */
	public void makeOrder(Client client, List<Product> products, List<Integer> quantities) {
		
		int sum = 0;
		int total = 0;
		try {
			writer = new PrintWriter("Bill" + billNumber + ".txt", "UTF-8");
		} catch (IOException e) {
		}

		writer.println("Name: " + client.getName());
		writer.println("--------------------------------------");	
		for(int i = 0; i < products.size(); ++i) {
			
			sum += products.get(i).getPrice() * quantities.get(i);
			Order order = new Order(orderBLL.getLastIndex() + 1, client.getClientId(), sum);
			orderBLL.insert(order);
			
			Contract contract = new Contract(products.get(i).getProductId(), order.getOrderId(), quantities.get(i));
			productBLL.decrementStock(products.get(i).getProductId(), quantities.get(i));
			contractBLL.insert(contract);
			
			writeBill(products.get(i).getName(), quantities.get(i), sum);
			total += sum;
			sum = 0;
		}		
		writer.println("Total cost: " + total + " $");
		billNumber++;
		writer.close();
	}	
}
