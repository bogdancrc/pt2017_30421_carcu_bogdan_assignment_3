package bll.validators;

import model.Client;
/**
 * Age constraint.
 * @author Carcu Bogdan
 * 
 */
public class AgeValidator implements Validator<Client> {

	private static final int MIN_AGE = 16;

	public void validate(Client t) {
		
		if(t.getAge() < MIN_AGE)
			throw new IllegalArgumentException("The Client Age limit is not respected!");
		
	}
	
}
