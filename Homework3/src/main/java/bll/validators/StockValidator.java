package bll.validators;

import model.Product;

/**
 * Stock constraint.
 * @author Carcu Bogdan
 * 
 */
public class StockValidator implements Validator<Product> {

	private static final int MAX_CAPACITY = 100;
	
	public void validate(Product t) {
		
		if(t.getStock() < 0)
			throw new IllegalArgumentException("Stock underflow");
		
		if(t.getStock() > MAX_CAPACITY)
			throw new IllegalArgumentException("Stock overflow");
		
	}

}
