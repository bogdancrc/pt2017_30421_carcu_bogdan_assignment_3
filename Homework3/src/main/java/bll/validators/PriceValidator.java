package bll.validators;

import model.Product;

/**
 * Price constraint.
 * @author Carcu Bogdan
 * 
 */
public class PriceValidator implements Validator<Product>{
	
	public void validate(Product t) {
		
		if(t.getPrice() <= 0)
			throw new IllegalArgumentException("Price limit is not respected!");
		
	}
	

}
