package bll.validators;

import model.Contract;

/**
 * Quantity constraint.
 * @author Carcu Bogdan
 * 
 */
public class QuantityValidator implements Validator<Contract> {

	public void validate(Contract t) {
		
		if(t.getQuantity() < 1)
			throw new IllegalArgumentException("Quantity limit is not respected!");
		
	}

}
