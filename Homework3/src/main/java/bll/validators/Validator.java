package bll.validators;
/**
 * Interface used for data validation: implementing a set of constraints.
 * @author Carcu Bogdan
 * @since May 2017
 * @param <T> Specifies the type that the validation focuses on
 * 
 */
public interface Validator<T> {

	public void validate(T t);
	
}