package bll.validators;
import model.Client;

/**
 * Name constraint.
 * @author Carcu Bogdan
 * 
 */
public class NameValidator implements Validator<Client>{

	private static final int MAX_LENGTH = 45;

	public void validate(Client t) {
		
		if(t.getName().length() > MAX_LENGTH)
			throw new IllegalArgumentException("Invalid name!");
		
	}
	
}
