package bll;

import java.util.List;
import java.util.NoSuchElementException;
import dao.OrderDAO;
import model.Order;

/**
 * 
 * Business logic level of the DAO counterpart.
 * 
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *  
 */

public class OrderBLL {

	private OrderDAO orderDAO;
	
	public OrderBLL() {
		
		orderDAO = new OrderDAO();
		
	}
	
	public Order findById(int id) {
		
		Order result = orderDAO.findOrder(id);
		if(result == null) 
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		return result;
	}
	
	public List<Order> findAll() {
		
		List<Order> orders = orderDAO.findAll();
		if(orders.isEmpty())
			throw new NoSuchElementException("No products were found.");
		return orders;
		
	}
	
	public void insert(Order o) {
		
		orderDAO.insertOrder(o);
	}
	
	public boolean delete(int id) {
		
		return orderDAO.deleteOrder(id);
		
	}
	
	public void update(int id, Order o) {
		
		orderDAO.updateOrder(id, o);	
		
	}
	
	/**
	 * @return The index of the last order inserted in the table
	 */
	public int getLastIndex() {
		
	if(orderDAO.findAll().size() == 0)
		return 0;
	else
		return orderDAO.findAll().get(orderDAO.findAll().size() - 1).getOrderId();
		
	}
	
}
