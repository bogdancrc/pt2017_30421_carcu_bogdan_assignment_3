package bll;

import java.util.List;
import java.util.NoSuchElementException;
import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.ContractDAO;
import model.Contract;

/**
 * 
 * Business logic level of the DAO counterpart.
 * 
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *  
 */

public class ContractBLL {

	private ContractDAO contractDAO;
	private Validator<Contract> validator;
	
	public ContractBLL() {
		
		contractDAO = new ContractDAO();
		validator = new QuantityValidator();
	
	}
	
	public Contract findById(int id) {
			
		Contract result = contractDAO.findContract(id);
		if(result == null) 
			throw new NoSuchElementException("The contract was not found!");
		return result;
	}
	
	public List<Contract> findAll() {
		
		List<Contract> contracts = contractDAO.findAll();
		if(contracts.isEmpty())
			throw new NoSuchElementException("No contracts were found.");
		return contracts;
		
	}
	
	public void insert(Contract c) {
		
		validator.validate(c);
		contractDAO.insertContract(c);
	}
	
	public boolean delete(int id) {
		
		return contractDAO.deleteContract(id);
		
	}
	
	public void update(int id, Contract c) {
		
		validator.validate(c);
		contractDAO.updateContract(id, c);	
		
	}
	
}
