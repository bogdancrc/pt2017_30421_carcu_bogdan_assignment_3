package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import bll.validators.AgeValidator;
import bll.validators.NameValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

public class ClientBLL {
	
	/**
	 * Business logic level of the DAO counterpart.
	 * 
	 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
	 * @since May 2017
	 *
	 */

	private ClientDAO clientDAO;
	private List<Validator<Client>> validators;
	
	public ClientBLL() {
		
		clientDAO = new ClientDAO();
		validators = new ArrayList<Validator<Client>>();
		validators.add(new AgeValidator());
		validators.add(new NameValidator());
		
	}
	
	public Client findById(int id) {
			
		Client result = clientDAO.findClient(id);
		if(result == null) 
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		return result;
	}
	
	public List<Client> findAll() {
		
		List<Client> clients = clientDAO.findAll();
		if(clients.isEmpty())
			throw new NoSuchElementException("No clients were found.");
		return clients;
		
	}
	
	public void insert(Client c) {
		
		for (Validator<Client> v : validators) {
			v.validate(c);
		}
		
		clientDAO.insertClient(c);
	}
	
	public boolean delete(int id) {
		
		return clientDAO.deleteClient(id);
		
	}
	
	public void update(int id, Client c) {
		
		for (Validator<Client> v : validators) {
			v.validate(c);
		}
		
		clientDAO.updateClient(id, c);	
	}
	
}
