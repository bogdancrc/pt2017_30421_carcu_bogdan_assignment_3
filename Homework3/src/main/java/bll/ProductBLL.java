package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import bll.validators.PriceValidator;
import bll.validators.StockValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

/**
 * 
 * Business logic level of the DAO counterpart.
 * @author: Carcu Bogdan, Group 30421, Technical University of Cluj-Napoca, Computer Science.
 * @since May 2017
 *  
 *
 */
public class ProductBLL {
	
	private ProductDAO productDAO;
	private List<Validator<Product>> validators;
	
	public ProductBLL() {
		
		productDAO = new ProductDAO();
		validators = new ArrayList<Validator<Product>>();
		validators.add(new PriceValidator());
		validators.add(new StockValidator());
		
	}
	
	public Product findById(int id) {
			
		Product result = productDAO.findProduct(id);
		if(result == null) 
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		return result;
	}
	
	public List<Product> findAll() {
		
		List<Product> products = productDAO.findAll();
		if(products.isEmpty())
			throw new NoSuchElementException("No products were found.");
		return products;
		
	}
	
	public void insert(Product p) {
		
		for (Validator<Product> v : validators) {
			v.validate(p);
		}
		
		productDAO.insertProduct(p);
	}
	
	public boolean delete(int id) {
		
		return productDAO.deleteProduct(id);
		
	}
	
	public void update(int id, Product p) {
		
		for (Validator<Product> v : validators) {
			v.validate(p);
		}
		
		productDAO.updateProduct(id, p);	
	}
	
	/**
	 * @param id The id of the element whose stock needs to be decreased
	 * @param amount The amount of items that need to be removed from stock due to a successful order
	 */
	public void decrementStock(int id, int amount) {
		
		Product result = productDAO.findProduct(id);
		if(result == null) 
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		
		int stock = result.getStock();
		result.setStock(stock - amount);
		
		for (Validator<Product> v : validators) {
			v.validate(result);
		}
		
		productDAO.update(id, result);
		
	}

}
